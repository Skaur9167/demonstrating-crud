# demonstrating-crud

This is a basic product to demonstrate all the crud operations for database using a webpage.

The webpage is created in PHP and talks to mySQL database at the backend.

### Pre-requisites for the Project

- Install Xampp
- Install MySql workbench
- Notepad ++

### Steps to build and run the project

- Run Xampp
  - Start Apache Server
  - Start MySql 
- Create database using MySql workbench using the `data/init.sql` file provided in the repository
- Open your favorite browser and goto `http://localhost:80` to see the homepage.

