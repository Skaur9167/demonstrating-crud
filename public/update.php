<?php
 ob_start();
   session_start();
   //connection to a database
	require_once('../config.php');
?>
<?php include "../template/header.php"; ?>
<style>
input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 100%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

div {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
table, td, th {  
  border: 1px solid #ddd;
  text-align: left;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 15px;
}

.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
}


.button2:hover {
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
}

h2 { color: #7c795d; font-family: 'Trocchi', serif; font-size: 45px; font-weight: normal; line-height: 48px; margin: 0; }

</style>
<h2>Update User by selecting edit</h2>
<div>
<table>
	<tr>
		<th>ID</th>
		<th>FirstName</th>
		<th>LastName</th>
		<th>Email</th>
		<th>Age</th>
		<th>Location</th>
		<th>Edit</th>
	</tr>
	<?php
		$sql = " SELECT * FROM users ";
		$result = mysqli_query($db,$sql);
		if (mysqli_num_rows($result) > 0) {
		while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
			echo "<tr>";
			printf("<td>%s</td>", $row["id"]);
			printf("<td>%s</td>", $row["firstname"]);
			printf("<td>%s</td>", $row["lastname"]);
			printf("<td>%s</td>", $row["email"]);
			printf("<td>%s</td>", $row["age"]);
			printf("<td>%s</td>", $row["location"]);
			printf('<td><a href="update-single.php?id=%s">Edit</a></td>',$row["id"]);
			echo "</tr>";
		 }
	}	
?>
</table>
</div>
<br>
<h3><a href="index.php">Back to Home</a></h3>
<?php include "../template/footer.php"; ?>