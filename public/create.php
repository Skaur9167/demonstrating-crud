<?php
 ob_start();
   session_start();
   //connection to a database
	require_once('../config.php');
?>
<?php
if (isset($_POST['submit']) != null){
	$firstname=$_POST['firstname'];
	$lastname=$_POST['lastname'];
	$email=$_POST['email'];
	$age=$_POST['age'];
	$location=$_POST['location'];
	
	$sql = "INSERT INTO users (firstname, lastname, email, age , location) VALUES ('$firstname', '$lastname','$email','$age','$location')";
	if (mysqli_query($db, $sql)) {
					$error= "New record created successfully";
				} else {
					$error= "Insert Failure!";
				}
			}else {
				$error="Missing Fields! Unable to Insert new Row !";
			}
?>
<?php include "../template/header.php"; ?>
<style>
input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 100%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

div {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
table, td, th {  
  border: 1px solid #ddd;
  text-align: left;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 15px;
}

.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
}


.button2:hover {
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
}

h2 { color: #7c795d; font-family: 'Trocchi', serif; font-size: 45px; font-weight: normal; line-height: 48px; margin: 0; }

</style>
<h2>Add a new user</h2>

<form method="post">
	<label for="firstname">FirstName</label>
	<input type="text" name="firstname" id="firstname">
	<label for="lastname">LastName</label>
	<input type="text" name="lastname" id="lastname">
	<label for="email">Email Address</label>
	<input type="text" name="email" id="email">
	<label for="age">Age</label>
	<input type="text" name="age" id="age">
	<label for="location">Location</label>
	<input type="text" name="location" id="location">
	<input type="submit" name="submit" value="Submit">
</form>
<br>
<a href="index.php">Back to Homepage</a>
<?php include "../template/footer.php"; ?>
